import Qualitative_Numeric_Fashion_MNIST as q
import pandas as pd
import numpy as np
import os


q.FRWI_SAMPLES = 1000
q.LIME_SAMPLES = 1000

SAMPLES_ICS = 10
EXPERIMENT_FILEPATH_CSV = './quantitative_experiments.csv'

SAMPLES_BY_CLASS = 100


def interpretation_capacity_score(model_name,model,X_test,y_pred,lmis,indices):
    if model_name not in q.MODELS_NAMES:
        raise ValueError("Invalide model name. Use ones of these:",MODELS_NAMES)
    # import numpy as np

    c = 0
    p = 0
    n = 0

    cutted_img = np.zeros((len(indices),q.IMG_SIZE), dtype=np.uint8)
    cutted_img_reverse = np.zeros((len(indices),q.IMG_SIZE), dtype=np.uint8)

    for i,ix in enumerate(indices):
        lmi = lmis[i][:q.IMG_SIZE]
        cut = np.mean(lmi)+np.std(lmi)

        cc = 0
        for j in range(q.IMG_SIZE):
            if lmi[j] > cut:
                cc += 1
                cutted_img[i][j] = X_test[ix][j]
            else:
                cutted_img_reverse[i][j] = X_test[ix][j]
        
        c += cc/q.IMG_SIZE

    y_pred_cutted = q.predict_model(model_name,model,cutted_img)
    y_pred_cutted_reverse = q.predict_model(model_name,model,cutted_img_reverse)

    for i,ix in enumerate(indices):
        y = np.argmax(y_pred[i])
        p += 1 if y == np.argmax(y_pred_cutted[i]) else 0
        n += 1 if y != np.argmax(y_pred_cutted_reverse[i]) else 0

    c /= len(indices)
    p /= len(indices)
    n /= len(indices)

    ics = 0
    if p+n != 0:
        ics = (1-c)*2*((p*n)/(p+n))

    return ics,c,p,n


def single_experiment(dataset_name,data,model_name,explainers,indices,verbose=True):
    X,y, X_test, y_test = data
    frwi, lime = explainers

    if verbose:
        print("training",model_name,"...")
    model = q.fit_model(model_name,X,y)

    if verbose:
        print("predicting",model_name,'...')
    y_pred = q.predict_model(model_name,model,X_test)

    acc = q.accuracy(model_name,y_pred,y_test)
    if verbose:
        print("accuracy from",model_name,':',acc)

    if verbose:
        print("loading frwi explanation...")
    lmis_frwi = q.get_frwi_explanations(model_name,frwi,model,X_test,indices)

    if verbose:
        print("calculating frwi ics...")
    ics_frwi,c_frwi,p_frwi,n_frwi = interpretation_capacity_score(model_name,model,X_test,y_pred,lmis_frwi,indices)

    if verbose:
        print("loading lime explanations...")
    lmis_lime = q.get_lime_explanations(model_name,lime,model,X_test,y_test,indices)

    if verbose:
        print("calculating lime ics...")
    ics_lime,c_lime,p_lime,n_lime = interpretation_capacity_score(model_name,model,X_test,y_pred,lmis_lime,indices)

    return {'frwi':[ics_frwi,c_frwi,p_frwi,n_frwi], 'lime': [ics_lime,c_lime,p_lime,n_lime], 'acc': acc}


def save_sample(experiments,ix,dataset_name,model_name,sample,filename=EXPERIMENT_FILEPATH_CSV):
    for key in ['frwi', 'lime']:
        experiments.loc[ix,'explainer'] = key
        experiments.loc[ix,'dataset'] = dataset_name
        experiments.loc[ix,'model'] = model_name

        experiments.loc[ix,'accuracy'] = sample['acc']

        for i,v in enumerate(['ics','c','p','n']):
            experiments.loc[ix,v] = sample[key][i]
        ix += 1
    
    experiments.to_csv(filename, index=False)
    return ix

def load_datasets():
    datasets = {}
    for dataset_name in q.DATASET_NAMES:
        datasets[dataset_name] = q.load_dataset(dataset_name)
        y_test = datasets[dataset_name][3]
        datasets[dataset_name+"_i"] = q.get_indices(y_test,samples=SAMPLES_BY_CLASS)
    return datasets


def load_experiments(filename):
    experiments = None
    NUMBER_OF_EXPLAINERS = 2
    total_samples = NUMBER_OF_EXPLAINERS*len(q.DATASET_NAMES)*len(q.MODELS_NAMES)*SAMPLES_ICS

    if os.path.exists(filename):
        experiments = pd.read_csv(filename)
        if len(experiments) != total_samples:
            experiments = None

    if experiments is None:
        experiments = pd.DataFrame({
            'ix': [ i for i in range(total_samples)],
            'dataset': ['' for i in range(total_samples)],
            'model': ['' for i in range(total_samples)],
            'explainer': ['' for i in range(total_samples)],
            'accuracy': np.zeros(total_samples),
            'ics': np.zeros(total_samples),
            'c': np.zeros(total_samples),
            'p': np.zeros(total_samples),
            'n': np.zeros(total_samples),
        })
    experiments.to_csv(filename, index=False)
    return experiments

def load_tasks(experiments):
    tasks = []
    ix = 0 # experiment index
    for dataset_name in q.DATASET_NAMES:
        for model_name in q.MODELS_NAMES:
            for i in range(SAMPLES_ICS):
                if experiments.loc[ix, 'explainer'] != 'frwi':
                    task = {
                        'dataset_name': dataset_name,
                        'model_name': model_name,
                        'sample': i,
                        'ix': ix
                    }
                    tasks.append(task)
                ix += 2
    return tasks


if __name__ == "__main__":
    import sys
    from tqdm import tqdm

    if len(sys.argv) == 2:
        EXPERIMENT_FILEPATH_CSV = sys.argv[1] + '.csv'
    print("experiment file output:", EXPERIMENT_FILEPATH_CSV)

    experiments = load_experiments(EXPERIMENT_FILEPATH_CSV)

    print("loading datasets...")
    datasets = load_datasets()

    print("loading explainers...")
    explainers = q.load_explainers()

    print("loading tasks...")
    tasks = load_tasks(experiments)

    print("executing experiments...")
    for task in tqdm(tasks,desc="tasks"):
        dataset_name = task['dataset_name']
        model_name = task['model_name']
        ix = task['ix']

        sample = single_experiment(dataset_name, datasets[dataset_name], model_name,explainers, datasets[dataset_name+"_i"],verbose=False)
        save_sample(experiments, ix, dataset_name, model_name, sample)
