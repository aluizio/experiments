import generate_negatives_lmis as gn
import Qualitative_Numeric_Fashion_MNIST as ql
import pool_quantitative_experiments as p2
from multiprocessing import Pool

from xaiwann import FRWI
import os
from tqdm import tqdm


if __name__ == "__main__":
    print("loading dataset...")
    X,y, X_test, y_test = gn.load_dataset()
    
    print("loading explainers...")
    frwi, lime = ql.load_explainers()
    
    print("checking folders...")
    if not os.path.exists(gn.IMAGES_FOLDER):
        os.mkdir(gn.IMAGES_FOLDER)
    
    folders = {}
    for m in ql.MODELS_NAMES:
        fd = os.path.join(gn.IMAGES_FOLDER, "parameters_"+m)
        folders[m] = fd
        if not os.path.exists(fd):
            os.mkdir(fd)
    
    print("loading tasks...")
    samples = [1000, 5000, 10000, 50000, 100000]
    windows = [1, 2, 4, 6, 8, 10]

    tasks = []
    models = {}
    for m in ql.MODELS_NAMES:
        print("training",m,"...")
        # if m != "wisard":
        #     continue
        model = ql.fit_model(m, X, y)
        models[m] = model

        for s in samples:
            for w in windows:
                task = {
                    "model_name": m,
                    "sample": s,
                    "window": w,
                }
                tasks.append(task)
    
    img = X_test[0] # it is a seven
    def gen_exp(task):
        ax, fig = ql.load_frame(200,200)

        model_name = task['model_name']
        frwi = FRWI(task['window'],ql.IMG_HEIGHT,ql.IMG_WIDTH)

        t = [model_name, models[model_name], img, 0]
        frwi = p2.get_frwi_explanation(frwi,*t,samples=task['sample'])
        lmi = frwi[:ql.IMG_SIZE].reshape(ql.IMG_HEIGHT,ql.IMG_WIDTH)

        name = "window_"+str(task['window'])+"_sample_"+str(task['sample'])+".png"
        filename = os.path.join(folders[model_name], name)
        ql.save_img(ax, fig, filename, lmi)

        fig.clf()

    print("generating images...")
    with Pool(processes=os.cpu_count()) as pool:
        results = pool.imap_unordered(gen_exp, tasks)
        with tqdm(total=len(tasks), desc="images") as pbar:
            for i in results:
                pbar.update()
