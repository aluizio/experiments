from datasets.mnist_noise_images.loader import loadData
from datasets.mnist.loader import load_data
from multiprocessing import Pool

import Qualitative_Numeric_Fashion_MNIST as ql
import pool_quantitative_experiments as p2

from tqdm import tqdm

import numpy as np
import os

MNIST_FOLDER = './datasets/mnist'
MNIST_NOISE_FOLDER = 'datasets/mnist_noise_images'
IMG_SIZE = 784

ql.FRWI_SAMPLES = 100000
ql.LIME_SAMPLES = ql.FRWI_SAMPLES

IMAGES_FOLDER = "./images"
NEGATIVE_IMAGES_FOLDER = os.path.join(IMAGES_FOLDER, 'negatives')


def load_dataset():
    X,y, X_test, y_test = load_data(path=MNIST_FOLDER)

    X = X.reshape(-1,IMG_SIZE)
    y = np.array(y,dtype=np.float32)

    X_test = X_test.reshape(-1,IMG_SIZE)
    y_test = np.array(y_test,dtype=np.float32)

    return X,y, X_test, y_test

def get_lime_negative(lmi):
    return np.array([ (1 if p==-1 else 0) for p in lmi],dtype=np.float32)

def get_lime_positive(lmi):
    return np.array([ (1 if p==1 else 0) for p in lmi],dtype=np.float32)


def gen_exp(task):
    model_name = task['model_name']
    i = task['i']

    frwi, lime = ql.load_explainers()
    ax, fig = ql.load_frame(200,200)

    t = [model_name, models[model_name], task['x'], task['y']]
    lime_lmi = p2.get_lime_explanation(lime,*t,positive_only=False)
    frwi_lmi = p2.get_frwi_explanation(frwi,*t)

    frwi_positive = frwi_lmi[:IMG_SIZE].reshape(ql.IMG_HEIGHT,ql.IMG_WIDTH)
    frwi_negative = frwi_lmi[IMG_SIZE:].reshape(ql.IMG_HEIGHT,ql.IMG_WIDTH)

    lime_positive = get_lime_positive(lime_lmi).reshape(ql.IMG_HEIGHT,ql.IMG_WIDTH)
    lime_negative = get_lime_negative(lime_lmi).reshape(ql.IMG_HEIGHT,ql.IMG_WIDTH)

    # pred = "_pred_"+str(task['y'])
    name = "lime_"+model_name+"_"+y_noise[i]+".png"
    filename = os.path.join(NEGATIVE_IMAGES_FOLDER, "positive_"+name)
    ql.save_img(ax, fig, filename, lime_positive)

    filename = os.path.join(NEGATIVE_IMAGES_FOLDER, "negative_"+name)
    ql.save_img(ax, fig, filename, lime_negative)

    name = "frwi_"+model_name+"_"+y_noise[i]+".png"
    filename = os.path.join(NEGATIVE_IMAGES_FOLDER, "positive_"+name)
    ql.save_img(ax, fig, filename, frwi_positive)

    filename = os.path.join(NEGATIVE_IMAGES_FOLDER, "negative_"+name)
    ql.save_img(ax, fig, filename, frwi_negative)

    fig.clf()

if __name__ == "__main__":
    print("loading dataset...")
    X,y, X_test, y_test = load_dataset()

    print("loading noising data...")
    data_noise, y_noise = loadData(path=MNIST_NOISE_FOLDER)
    data_noise = data_noise.reshape(-1,IMG_SIZE)
    
    print("checking folders...")
    if not os.path.exists(IMAGES_FOLDER):
        os.mkdir(IMAGES_FOLDER)
    if not os.path.exists(NEGATIVE_IMAGES_FOLDER):
        os.mkdir(NEGATIVE_IMAGES_FOLDER)
    
    print("saving orignal images...")
    ax, fig = ql.load_frame(200,200)
    for i in tqdm(range(len(data_noise))):
        img = data_noise[i].reshape(ql.IMG_HEIGHT,ql.IMG_WIDTH)
        class_name = "original_"+str(y_noise[i])+'.png'
        filename = os.path.join(NEGATIVE_IMAGES_FOLDER,class_name)
        ql.save_img(ax,fig,filename,img)
    fig.clf()

    print("loading tasks...")
    tasks = []
    models = {}
    for model_name in ql.MODELS_NAMES:
        print("training",model_name,"...")
        model = ql.fit_model(model_name, X, y)
        models[model_name] = model

        print("predicting data noise...")
        y_pred = ql.predict_model(model_name,model,data_noise)

        print("preparing tasks...")
        for i in range(len(y_pred)):
            task = {
                "i": i,
                "model_name": model_name,
                "x": data_noise[i],
                "y": np.argmax(y_pred[i]),
            }
            tasks.append(task)

    print("generating images...")
    with Pool(processes=os.cpu_count()) as pool:
        results = pool.imap_unordered(gen_exp, tasks)
        with tqdm(total=len(tasks), desc="images") as pbar:
            for i in results:
                pbar.update()

