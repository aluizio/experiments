from PIL import Image
import numpy as np
import os

def loadData(path=""):
    baseName = "_original"
    types = ["","_p","_e","_m"]
    data = []
    y = []
    for i in range(10):
        for t in types:
            filename = os.path.join(path,str(i)+baseName+t+".png")
            img = Image.open(filename)
            d = np.asarray(img,dtype=np.uint8)
            y.append(str(i)+t)
            data.append(d)
    return np.array(data), np.array(y)

if __name__ == "__main__":
    data = loadData()
    print(data.shape)