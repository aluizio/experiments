FROM ubuntu:18.04 
# lime only works on python3.7 or lesser because of pillow

WORKDIR /notebooks

ADD Pipfile /notebooks/

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

RUN apt update && apt install -y \
    python3 \
    python3-pip \
    git && \
    pip3 install --upgrade pip && \
    pip3 install pipenv
    

RUN pipenv lock && set -ex && pipenv install --deploy --system && \
    jt -t monokai && \
    jupyter nbextension enable --py widgetsnbextension

EXPOSE 8888

ENTRYPOINT python3 -m notebook --notebook-dir=/notebooks --ip=0.0.0.0 --allow-root
