import numpy as np

import wisardpkg as wp

from sklearn.preprocessing import OneHotEncoder
from sklearn import linear_model

from sklearn.pipeline import Pipeline
from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import Normalizer

import sklearn.metrics as metrics

from tqdm import tqdm
from PIL import Image

import matplotlib.pyplot as plt

import sys,os

# FRWI
from xaiwann import FRWI

#LIME
from lime import lime_image
from lime.wrappers.scikit_image import SegmentationAlgorithm
from skimage.color import gray2rgb, rgb2gray, label2rgb # since the code wants color images



MNIST_FOLDER = './datasets/mnist/'
FASHION_FOLDER = './datasets/fashion_mnist/'
DATASETS_FOLDER = './datasets'

TRAIN_SAMPLES = 60000
TEST_SAMPLES = 10000

IMG_HEIGHT = 28
IMG_WIDTH = 28
IMG_SIZE = IMG_HEIGHT*IMG_WIDTH

DATASET_NAMES = ['numeric', 'fashion'] # do not change the order
MODELS_NAMES = ['wisard', 'ridge_regression', 'random_forest'] # do not change the order

IMAGES_FOLDER = "images"

# frwi parameters
FRWI_WINDOW = 4
FRWI_SAMPLES = 10000
FRWI_GREY_VALUE = 0

# lime parameters
LIME_SAMPLES = 10000
LIME_GREY_VALUE = 0

def blockTqdm():
    sys.stderr = open(os.devnull, 'w')

def enableTqdm():
    sys.stderr = sys.__stderr__

def load_dataset(name):
    if name not in DATASET_NAMES:
        raise ValueError("Invalid dataset name. Use one of these:",DATASET_NAMES)

    sys.path.insert(1, DATASETS_FOLDER)
    
    if name == DATASET_NAMES[0]:
        from mnist.loader import load_data
        X,y, X_test, y_test = load_data(MNIST_FOLDER)
    
    if name == DATASET_NAMES[1]:
        from fashion_mnist.loader import load_data
        X,y, X_test, y_test = load_data(FASHION_FOLDER)

    X = X.reshape(TRAIN_SAMPLES,IMG_SIZE)
    y = np.array(y,dtype=np.float32)

    X_test = X_test.reshape(TEST_SAMPLES,IMG_SIZE)
    y_test = np.array(y_test,dtype=np.float32)

    return X,y, X_test, y_test


def fit_model(name, X, y):
    if name not in MODELS_NAMES:
        raise ValueError("Invalide model name. Use ones of these:",MODELS_NAMES)

    if name == MODELS_NAMES[0]:
        # import wisardpkg as wp
        bin = wp.MeanThreshold(4)
        wsd = wp.Wisard(IMG_SIZE,40,binarizer=bin)
        wsd.train(X,y)
        return wsd

    if name == MODELS_NAMES[1]:
        # from sklearn.preprocessing import OneHotEncoder
        # from sklearn import linear_model

        yp = OneHotEncoder()
        y2 = y.reshape(TRAIN_SAMPLES,1)
        y_temp = yp.fit_transform(y2).toarray()
        linear = linear_model.Ridge(alpha = 0)
        linear.fit(X,y_temp)
        return linear

    if name == MODELS_NAMES[2]:
        # from sklearn.pipeline import Pipeline
        # from sklearn.ensemble import RandomForestClassifier
        # from sklearn.preprocessing import Normalizer

        random_forest = Pipeline([
            ('Normalize', Normalizer()),
            ('RF', RandomForestClassifier())
        ])
        random_forest.fit(X,y)
        return random_forest


def predict_model(name, model, X):
    if name not in MODELS_NAMES:
        raise ValueError("Invalide model name. Use ones of these:",MODELS_NAMES)

    if name == MODELS_NAMES[0]:
        y_pred = model.predict(X)
        return y_pred

    if name == MODELS_NAMES[1]:
        y_pred = model.predict(X)
        return y_pred

    if name == MODELS_NAMES[2]:
        y_pred = model.predict_proba(X)
        return y_pred



def accuracy(name, y_pred, y):
    if name not in MODELS_NAMES:
        raise ValueError("Invalide model name. Use ones of these:",MODELS_NAMES)
    # import sklearn.metrics as metrics

    if name == MODELS_NAMES[0]:
        return metrics.accuracy_score(np.argmax(y_pred,axis=1),y)

    if name == MODELS_NAMES[1]:
        return metrics.accuracy_score(np.argmax(y_pred,axis=1),y.reshape(TEST_SAMPLES,1))

    if name == MODELS_NAMES[2]:
        return metrics.accuracy_score(np.argmax(y_pred,axis=1),y)


def load_explainers():
    # from xaiwann import FRWI

    frwi = FRWI(FRWI_WINDOW,IMG_HEIGHT,IMG_WIDTH)

    # from lime import lime_image
    # from lime.wrappers.scikit_image import SegmentationAlgorithm


    explainer = lime_image.LimeImageExplainer(verbose = False)
    segmenter = SegmentationAlgorithm('quickshift', kernel_size=1, max_dist=200, ratio=0.2)
    lime = [explainer,segmenter]

    return frwi, lime


def get_frwi_explanations(model_name,frwi,model,X_test,indices):
    if model_name not in MODELS_NAMES:
        raise ValueError("Invalide model name. Use ones of these:",MODELS_NAMES)
    # from tqdm import tqdm
    # from PIL import Image


    lmis = []
    for i in tqdm(indices):
        img = np.array(X_test[i],np.uint8)
        data = frwi.generatePermutations(img, FRWI_GREY_VALUE, FRWI_SAMPLES)

        if model_name == MODELS_NAMES[0]:
            c = model.predict(np.array([img]))
            responses = model.predict(data)

        if model_name == MODELS_NAMES[1]:
            c = np.array(model.predict(np.array([img]).reshape(1,IMG_SIZE)),dtype=np.float32)
            responses = np.array(model.predict(data.reshape(FRWI_SAMPLES,IMG_SIZE)),dtype=np.float32)

        if model_name == MODELS_NAMES[2]:
            c = np.array(model.predict_proba(np.array([img]).reshape(1,IMG_SIZE)),dtype=np.float32)
            responses = np.array(model.predict_proba(data.reshape(FRWI_SAMPLES,IMG_SIZE)),dtype=np.float32)

        lmi = frwi.explain(responses,c)
        lmis.append(lmi)
    return lmis

def get_lime_explanations(model_name,lime,model,X_test,y_test,indices):
    if model_name not in MODELS_NAMES:
        raise ValueError("Invalide model name. Use ones of these:",MODELS_NAMES)
    # from skimage.color import gray2rgb, rgb2gray, label2rgb # since the code wants color images

    if model_name == MODELS_NAMES[0]:
        def classifier(data):
            dd = np.array(np.stack([ rgb2gray(d) for d in data],0)*255,dtype=np.uint8).reshape((-1,784))
            return model.predict(dd)

    if model_name == MODELS_NAMES[1]:
        def classifier(data):
            dd = np.array(np.stack([ rgb2gray(d) for d in data],0)*255,dtype=np.uint8).reshape((-1,784))
            return model.predict(dd)

    if model_name == MODELS_NAMES[2]:
        def classifier(data):
            dd = np.array(np.stack([ rgb2gray(d) for d in data],0)*255,dtype=np.uint8).reshape((-1,784))
            return model.predict_proba(dd)

    lmis = []
    explainer, segmenter = lime
    for i in tqdm(indices):
        img_raw = gray2rgb(X_test[i]).reshape((IMG_HEIGHT,IMG_WIDTH,3))

        blockTqdm()
        explanation = explainer.explain_instance(img_raw, 
            classifier_fn = classifier, segmentation_fn=segmenter,
            top_labels=10, hide_color=LIME_GREY_VALUE, num_samples=LIME_SAMPLES, batch_size=1000)
        enableTqdm()

        _, mask = explanation.get_image_and_mask(y_test[i], 
        positive_only=True, num_features=10, hide_rest=False, min_weight = 0.01)

        lmis.append(np.array(mask,dtype=np.float32).reshape(IMG_SIZE))
    return lmis

def get_indices(y_test, samples=1):
    classes = {}
    indices = []
    for i,v in enumerate(y_test):
        if v not in classes:
            classes[v] = 1
            indices.append(i)
        elif classes[v] < samples:
            classes[v] += 1
            indices.append(i)
    return indices

def load_frame(w,h):
    # import matplotlib.pyplot as plt
    fig = plt.figure(frameon=False)
    fig.set_size_inches(w,h)
    ax = plt.Axes(fig, [0., 0., 1., 1.])
    ax.set_axis_off()
    fig.add_axes(ax)
    return ax, fig

def save_img(ax,fig,filename,x,dpi=2):
    original = np.reshape(x,(28,28))
    ax.imshow(original, aspect='auto')
    fig.savefig(filename,dpi=dpi)


if __name__ == "__main__":
    ax, fig = load_frame(200,200)
    print("loading explainers...")
    frwi, lime = load_explainers()

    for dataset_name in DATASET_NAMES:
        print("loading dataset",dataset_name,"...")
        X,y, X_test, y_test = load_dataset(dataset_name)

        print("getting indices to generate explanations...")
        indices = get_indices(y_test)

        base_folder = os.path.join(IMAGES_FOLDER,"positives_"+dataset_name)
        if not os.path.exists(base_folder):
            os.mkdir(base_folder)

        print("saving orignal images...")
        for i in tqdm(indices):
            class_name = "original_"+str(int(y_test[i]))+'.png'
            filename = os.path.join(base_folder,class_name)
            save_img(ax,fig,filename,X_test[i])


        for model_name in MODELS_NAMES:
            print("training",model_name,"...")
            model = fit_model(model_name,X,y)

            print("predicting",model_name,'...')
            y_pred = predict_model(model_name,model,X_test)

            acc = accuracy(model_name,y_pred,y_test)
            print("accuracy from",model_name,':',acc)

            print("loading lime explanations...")
            lmis_lime = get_lime_explanations(model_name,lime,model,X_test,y_test,indices)

            print("loading frwi explanation...")
            lmis_frwi = get_frwi_explanations(model_name,frwi,model,X_test,indices)

            print("saving frwi/lime images explanations...")
            with tqdm(total=len(indices)) as pbar:
                for i,ix in enumerate(indices):
                    lmi_frwi = lmis_frwi[i]
                    lmi_lime = lmis_lime[i]
                    class_name = model_name+"_"+str(int(y_test[ix]))+'.png'
                    
                    filename = os.path.join(base_folder,"frwi_"+class_name)
                    save_img(ax,fig,filename,lmi_frwi[:IMG_SIZE])

                    filename = os.path.join(base_folder,"lime_"+class_name)
                    save_img(ax,fig,filename,lmi_lime)

                    pbar.update()