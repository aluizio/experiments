from PIL import Image, ImageDraw, ImageFont
from multiprocessing import Pool
from tqdm import tqdm
import os

IMAGES_FOLDER = "images"

def load_positives_grid(folder):
    lines = ["original",
    "frwi_wisard","lime_wisard", 
    "frwi_ridge_regression", "lime_ridge_regression",
    "frwi_random_forest", "lime_random_forest"]
    text_lines = ["original",
    "frwi wisard","lime wisard", 
    "frwi ridge regression", "lime ridge regression",
    "frwi random forest", "lime random forest"]
    columns = [0,1,2,3,4,5,6,7,8,9]
    def get_filename(l,c):
        return l+"_"+str(c)+".png"
    outputfile = os.path.join(IMAGES_FOLDER,folder+"_grid.png")
    return {
        'base_folder': os.path.join(IMAGES_FOLDER,folder),
        'outputfile': outputfile,
        'lines': lines,
        'columns': columns,
        'text_lines': text_lines,
        'text_columns': columns,
        'get_filename': get_filename,
        'size': (400,400),
        'grid_thickness': 10,
        'grid_color': (0,0,0,255),
        'grid_ext_width': 650,
        'grid_ext_height': 100,
        'grid_ext_color': (255,255,255,255),
        'text_color': (0,0,0,255),
        'text_width': 100,
        'text_height': 30,
        'text_size': 50
    }

def load_parameters_grid(folder):
    BASE_FOLDER = os.path.join("images",folder)
    lines = [1,2,4,6,8,10]
    columns = [1000,5000,10000,50000,100000]
    def get_filename(l,c):
        return "window_"+str(w)+"_sample_"+str(s)+".png"
    outputfile = os.path.join(IMAGES_FOLDER,folder+"_grid.png")
    return {
        'base_folder': BASE_FOLDER,
        'outputfile': outputfile,
        'lines': lines,
        'columns': columns,
        'text_lines': lines,
        'text_columns': columns,
        'get_filename': get_filename,
        'size': (400,400),
        'grid_thickness': 10,
        'grid_color': (0,0,0,255),
        'grid_ext_width': 200,
        'grid_ext_height': 100,
        'grid_ext_color': (255,255,255,255),
        'text_color': (0,0,0,255),
        'text_width': 50,
        'text_height': 30,
        'text_size': 50
    }

def load_negatives_grid():
    BASE_FOLDER = os.path.join(IMAGES_FOLDER,"negatives")
    lines = []
    types = ["","_p","_e","_m"]
    for i in range(10):
        for t in types:
            lines.append(str(i)+t)
    columns = [
        "original",
        "positive_frwi_wisard",
        "negative_frwi_wisard",
        "positive_lime_wisard",
        "negative_lime_wisard",

        # "positive_frwi_ridge_regression",
        # "negative_frwi_ridge_regression",
        # "positive_lime_ridge_regression",
        # "negative_lime_ridge_regression",

        # "positive_frwi_random_forest",
        # "negative_frwi_random_forest",
        # "positive_lime_random_forest",
        # "negative_lime_random_forest"
    ]
    text_columns = [
        "original",
        "positive frwi",
        "negative frwi",
        "positive lime",
        "negative lime",
    ]
    def get_filename(l,c):
        return c+"_"+l+".png"
    outputfile = os.path.join(IMAGES_FOLDER,"wisard_negatives_grid.png")
    return {
        'base_folder': BASE_FOLDER,
        'outputfile': outputfile,
        'lines': lines,
        'columns': columns,
        'text_lines': lines,
        'text_columns': text_columns,
        'get_filename': get_filename,
        'size': (400,400),
        'grid_thickness': 10,
        'grid_color': (0,0,0,255),
        'grid_ext_width': 200,
        'grid_ext_height': 100,
        'grid_ext_color': (255,255,255,255),
        'text_color': (0,0,0,255),
        'text_width': 100,
        'text_height': 30,
        'text_size': 50
    }


if __name__ == "__main__":
    # params = load_positives_grid('positives_numeric')
    # params = load_parameters_grid('parameters_wisard')
    params = load_negatives_grid()

    BASE_FOLDER = params['base_folder']
    outputfile = params['outputfile']
    lines = params['lines']
    columns = params['columns']
    text_lines = params['text_lines']
    text_columns = params['text_columns']
    get_filename = params['get_filename']

    # text_line = "windows"
    # text_line_width = 5
    # text_column = "samples"
    # text_column_height = 10

    size = params['size'] # (width,height)
    grid_thickness = params['grid_thickness']
    grid_color = params['grid_color']
    grid_ext_width = params['grid_ext_width']
    grid_ext_height = params['grid_ext_height']
    grid_ext_color = params['grid_ext_color']

    text_color = params['text_color']
    text_width = params['text_width']
    text_height = params['text_height']
    text_size = params['text_size']
    text_font = "fonts/Ubuntu-Medium.ttf"


    tasks = []
    for i,w in enumerate(lines):
        for j,s in enumerate(columns):
            filename = get_filename(w,s)
            tasks.append({
                "file": os.path.join(BASE_FOLDER, filename),
                "l": i*(size[0]+grid_thickness)+grid_thickness,
                "c": j*(size[1]+grid_thickness)+grid_thickness
            })

    
    width = len(columns)*(size[0]+grid_thickness)+grid_thickness
    height = len(lines)*(size[1]+grid_thickness)+grid_thickness
    grid_img = Image.new('RGBA',(width, height), color=grid_color)

    def get_img(task):
        task['img'] = Image.open(task['file'])
        return task

    with Pool(processes=os.cpu_count()) as pool:
        results = pool.imap_unordered(get_img, tasks)
        with tqdm(total=len(tasks),desc="images") as pbar:
            for t in results:
                grid_img.paste(t['img'],box=(t['c'],t['l']))
                pbar.update()
    
    # grid_img.save(os.path.join(IMAGES_FOLDER,"grid_only.png"))


    final_img = Image.new('RGBA',(width+grid_ext_width, height+grid_ext_height), color=grid_ext_color)
    final_img.paste(grid_img, box=(grid_ext_width,grid_ext_height))
    drawtext = ImageDraw.Draw(final_img)
    font = ImageFont.truetype(font=text_font,size=text_size)

    # drawtext.text((grid_ext_width+width//4,text_column_height),text_column,fill=text_color, font=font)
    # drawtext.text((text_line_width,grid_ext_height+height//4),text_line,fill=text_color, font=font)

    for i,c in enumerate(text_columns):
        s = i*(size[0]+grid_thickness) + grid_ext_width + grid_thickness + size[0]//4
        drawtext.text((s,text_height),str(c),fill=text_color, font=font)
    
    for j,l in enumerate(text_lines):
        s = j*(size[1]+grid_thickness) + grid_ext_height + grid_thickness + size[1]//4
        drawtext.text((text_width,s),str(l),fill=text_color, font=font)

    final_img.save(outputfile)


