from multiprocessing import Pool

from tqdm import tqdm

from skimage.color import gray2rgb, rgb2gray, label2rgb # since the code wants color images

import Qualitative_Numeric_Fashion_MNIST as ql
import Quantitative_Numeric_Fashion_MNIST as qt
import sys, os
import numpy as np

EXPERIMENT_FILEPATH_CSV = './p2_quantitative_experiments.csv'

ql.FRWI_SAMPLES = 10000
ql.LIME_SAMPLES = ql.FRWI_SAMPLES

qt.SAMPLES_BY_CLASS = 10000
qt.SAMPLES_ICS = 10

def blockTqdm():
    sys.stderr = open(os.devnull, 'w')

def enableTqdm():
    sys.stderr = sys.__stderr__


def get_frwi_explanation(frwi,model_name,model,x,y,samples=ql.FRWI_SAMPLES):
    img = np.array(x,np.uint8)
    data = frwi.generatePermutations(img, ql.FRWI_GREY_VALUE, samples)

    if model_name == ql.MODELS_NAMES[0]:
        c = model.predict(np.array([img]))
        responses = model.predict(data)

    if model_name == ql.MODELS_NAMES[1]:
        c = np.array(model.predict(np.array([img]).reshape(1,ql.IMG_SIZE)),dtype=np.float32)
        responses = np.array(model.predict(data.reshape(-1,ql.IMG_SIZE)),dtype=np.float32)

    if model_name == ql.MODELS_NAMES[2]:
        c = np.array(model.predict_proba(np.array([img]).reshape(1,ql.IMG_SIZE)),dtype=np.float32)
        responses = np.array(model.predict_proba(data.reshape(-1,ql.IMG_SIZE)),dtype=np.float32)

    return frwi.explain(responses,c)


def get_lime_explanation(lime,model_name,model,x,y,positive_only=True):
    if model_name == ql.MODELS_NAMES[0]:
        def classifier(data):
            dd = np.array(np.stack([ rgb2gray(d) for d in data],0)*255,dtype=np.uint8).reshape((-1,784))
            return model.predict(dd)

    if model_name == ql.MODELS_NAMES[1]:
        def classifier(data):
            dd = np.array(np.stack([ rgb2gray(d) for d in data],0)*255,dtype=np.uint8).reshape((-1,784))
            return model.predict(dd)

    if model_name == ql.MODELS_NAMES[2]:
        def classifier(data):
            dd = np.array(np.stack([ rgb2gray(d) for d in data],0)*255,dtype=np.uint8).reshape((-1,784))
            return model.predict_proba(dd)

    explainer, segmenter = lime

    img_raw = gray2rgb(x).reshape((ql.IMG_HEIGHT,ql.IMG_WIDTH,3))

    blockTqdm()
    explanation = explainer.explain_instance(img_raw, 
        classifier_fn = classifier, segmentation_fn=segmenter,
        top_labels=10, hide_color=ql.LIME_GREY_VALUE, num_samples=ql.LIME_SAMPLES, batch_size=1000)
    enableTqdm()

    _, mask = explanation.get_image_and_mask(y,
    positive_only=positive_only, num_features=10, hide_rest=False, min_weight = 0.01)

    return np.array(mask,dtype=np.float32).reshape(ql.IMG_SIZE)


if __name__ == "__main__":

    if len(sys.argv) == 2:
        EXPERIMENT_FILEPATH_CSV = sys.argv[1] + '.csv'
    print("experiment file output:", EXPERIMENT_FILEPATH_CSV)

    experiments = qt.load_experiments(EXPERIMENT_FILEPATH_CSV)

    print("loading datasets...")
    datasets = qt.load_datasets()

    print("loading tasks...")
    tasks = qt.load_tasks(experiments)
    # tasks.reverse()

    print("executing experiments...")
    for task in tqdm(tasks, desc="tasks"):
        dataset_name = task['dataset_name']
        model_name = task['model_name']
        ix = task['ix']

        X,y, X_test, y_test = datasets[dataset_name]
        indices = datasets[dataset_name+"_i"]

        print("training",model_name,"...")
        model = ql.fit_model(model_name,X,y)

        print("predicting",model_name,'...')
        y_pred = ql.predict_model(model_name,model,X_test)

        acc = ql.accuracy(model_name,y_pred,y_test)
        print("accuracy from",model_name,':',acc)

        print("loading frwi explanation...")
        inner_tasks = []
        for i in indices:
            inner_tasks.append({
                'x': X_test[i],
                'y': np.argmax(y_pred[i])
            })

        def gen_exp(t):
            frwi, lime = ql.load_explainers()
            lime_lmi = get_lime_explanation(lime,model_name,model,t['x'],t['y'])
            frwi_lmi = get_frwi_explanation(frwi,model_name,model,t['x'],t['y'])
            return frwi_lmi, lime_lmi

        lmis_frwi = []
        lmis_lime = []

        with Pool(processes=os.cpu_count()) as pool:
            results = pool.imap_unordered(gen_exp, inner_tasks)
            with tqdm(total=len(inner_tasks), desc="images") as pbar:
                for f,l in results:
                    lmis_frwi.append(f)
                    lmis_lime.append(l)
                    pbar.update()

        print("")
        print("calculating frwi ics...")
        ics_frwi,c_frwi,p_frwi,n_frwi = qt.interpretation_capacity_score(model_name,model,X_test,y_pred,lmis_frwi,indices)

        print("calculating lime ics...")
        ics_lime,c_lime,p_lime,n_lime = qt.interpretation_capacity_score(model_name,model,X_test,y_pred,lmis_lime,indices)

        sample = {'frwi':[ics_frwi,c_frwi,p_frwi,n_frwi], 'lime': [ics_lime,c_lime,p_lime,n_lime], 'acc': acc}

        qt.save_sample(experiments, ix, dataset_name, model_name, sample, filename=EXPERIMENT_FILEPATH_CSV)