import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt


INPUT_CSV = "./qe_1k_exp_1k_samples.csv"
GRAPHICS_FOLDER = "./graphics"

DATASETS_NAMES = ['numeric', 'fashion']
COLUMNS = ['ics', 'c', 'p', 'n']


if __name__ == "__main__":
    import sys, os
    if len(sys.argv) == 2:
        INPUT_CSV = sys.argv[1]
    print("reading from:", INPUT_CSV)

    experiments = pd.read_csv(INPUT_CSV)

    if not os.path.exists(GRAPHICS_FOLDER):
        os.mkdir(GRAPHICS_FOLDER)
    
    folder_experiment = os.path.join(GRAPHICS_FOLDER,INPUT_CSV[:-len(".csv")])

    if not os.path.exists(folder_experiment):
        os.mkdir(folder_experiment)
    
    
    for dataset in DATASETS_NAMES:
        print("generating",dataset,"accuracy graphic...")
        plot = sns.catplot(
            x="model", 
            y="accuracy",
            kind="bar", data=experiments.query("dataset == '"+dataset+"'"))
        
        filename = os.path.join(folder_experiment,dataset+"_accuracy.png")
        plot.savefig(filename)

        for c in COLUMNS:
            print("generating",dataset,c,"graphic...")
            plot = sns.catplot(
                x="model", 
                y=c,
                hue="explainer",
                kind="bar", data=experiments.query("dataset == '"+dataset+"'"))

            filename = os.path.join(folder_experiment,dataset+"_"+c+".png")
            plot.savefig(filename)


