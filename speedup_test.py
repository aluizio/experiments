from threading import Thread, Lock as TL
from multiprocessing import Process, Lock as PL
from time import time
from tqdm import tqdm

def multiply_vector(x,vector):
    total = 0
    for v in vector:
        total += x**v
    return total


def task_single(tasks,vector):
    for task in tqdm(tasks):
        total = multiply_vector(task,vector)


class TaskThread(Thread):

    def __init__(self,tasks,vector):
        Thread.__init__(self)
        self.vector = vector
        self.tasks = tasks
    
    def run(self):
        for task in tqdm(self.tasks):
            total = multiply_vector(task,self.vector)



class TaskProcess(Process):

    def __init__(self,tasks,vector):
        Process.__init__(self)
        self.vector = vector
        self.tasks = tasks
    
    def run(self):
        for task in tqdm(self.tasks):
            total = multiply_vector(task,self.vector)

def produce_vector(load):
    return [ i for i in range(load) ]

def produce_tasks(load):
    return [ i for i in range(load) ]

def get_tasks(i,cpus,tasks):
    t = []
    for j in range(i,len(tasks),cpus):
        t.append(tasks[j])
    return t


if __name__ == "__main__":
    import os,sys
    load_task = 100
    load_vector = 10000

    if len(sys.argv) == 2:
        load_task = int(sys.argv[1])
    if len(sys.argv) == 3:
        load_task = int(sys.argv[1])
        load_vector = int(sys.argv[2])
    
    print("help - parameters: 'load_task' 'load_vector'")
    print("parameters: load_task=",load_task,"load_vector=",load_vector)
    
    cpus = os.cpu_count()

    vector = produce_vector(load_vector)
    tasks = produce_tasks(load_task)

    print("testing single...")
    start = time()

    task_single(tasks, vector)

    end = time()
    single_time = end-start
    print("time:",single_time)

    print("testing thread...")
    start = time()

    threads = []
    for i in range(cpus):
        tt = get_tasks(i,cpus,tasks)
        threads.append(TaskThread(tt, vector))
    
    for i in range(cpus):
        threads[i].start()
    
    for i in range(cpus):
        threads[i].join()
    
    end = time()
    thread_time = end-start
    print("time:",thread_time)

    tasks = produce_tasks(load_task)


    print("testing process...")
    start = time()

    process = []
    for i in range(cpus):
        tt = get_tasks(i,cpus,tasks)
        process.append(TaskProcess(tt, vector))
    
    for i in range(cpus):
        process[i].start()
    
    for i in range(cpus):
        process[i].join()
    
    end = time()
    process_time = end-start
    print("time:",process_time)

    print("speedup thread:",single_time/thread_time)
    print("speedup process:",single_time/process_time)